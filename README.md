This is my oldest app.  
It was my first project that I ventured in  
python and, since it's inception, has grown.  
Nowadays it uses mysql to keep the passwds,
and a randomness module from python to generate  
the codes. Not probably the most secure setup,
but still a long way from the txt file with all
the password unencrypted.
